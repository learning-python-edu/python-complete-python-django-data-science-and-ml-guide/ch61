import sqlite3


def connect_to_db():
    db_name = 'sqlite_db.db'
    return sqlite3.connect(db_name)


def create_table(db_conn):
    sql_request = """
    CREATE TABLE IF NOT EXISTS courses (
        id integer PRIMARY KEY,
        title text NOT NULL,
        students_qty integer,
        reviews_qty integer
    );"""
    db_conn.execute(sql_request)


def insert_into_courses(db_conn, title, students_qty, reviews_qty):
    sql_request = """
    INSERT INTO courses 
        (title, students_qty, reviews_qty)
    VALUES (?, ?, ?)"""
    db_conn.execute(sql_request, (title, students_qty, reviews_qty))


def insert_multiple_courses(db_conn, courses_to_insert):
    for course in courses_to_insert:
        insert_into_courses(db_conn, *course)
    db_conn.commit()


def find_from_courses(db_conn, reviews_qty):
    sql_request = """
    SELECT 
        id, title, students_qty, reviews_qty
    FROM courses
    WHERE
        reviews_qty > ?"""
    sql_cursor = db_conn.execute(sql_request, [reviews_qty])
    # read row one by one from cursor
    # for record in sql_cursor:
    #     print(record)
    # or fetch all rows at once
    records = sql_cursor.fetchall()
    print(records)


if __name__ == '__main__':
    courses = [
        ('Java course', 35, 254),
        ('JavaScrip course', 15, 14),
        ('Kotlin course', 32, 41),
    ]
    with connect_to_db() as db_connection:
        create_table(db_connection)
        insert_into_courses(db_connection, 'Complete Python', 25, 124)
        insert_multiple_courses(db_connection, courses)
        find_from_courses(db_connection, 100)
